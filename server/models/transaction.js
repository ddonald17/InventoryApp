import mongoose from 'mongoose';

const TransSchema = mongoose.Schema({
    cust_name:{
        type: String,
        required: true
    },
    cust_address:{
        type: String
    },
    item: {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Product'
    },
    quantity:{
        type: Number,
        required: true
    },
    sell_price:{
        type: Number,
        required: true
    },
    discount:{
        type: Number,
        required: true
    },
    created_by:{
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'User'
    },
   profits:{
       type:Number,
       default:0,
   },
   total:{
        type: Number
   },
    timestamp:{
        type: Date,
        default: new Date()
    },
    
});

const Transaction = mongoose.model('Transaction',TransSchema);

export default Transaction;
