import mongoose from 'mongoose';

const CategorySchema = mongoose.Schema({
    item_name:{
        type: String,
        required: true
    },
    category_description:{
        type: String,
        required: true
    },
    tax:{
        type:String,
        required: true
    },
    created_by:{
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'User'
    },
});

const Category = mongoose.model('Category',CategorySchema);

export default Category;