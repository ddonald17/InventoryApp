import Transaction from '../models/transaction.js';
import Product from '../models/product.js';
import User from '../models/user.js';
import pdf from 'html-pdf';
import printReport from '../document/printreport.js'

export const addTrans = async (req, res) => {
    const { cust_name, cust_address, item, quantity, discount, sell_price,total, created_by} = req.body;
    console.log(req.body)

    const newTrans = new Transaction({ cust_name, cust_address, item, quantity, discount, sell_price, total, created_by });
     console.log(newTrans)
    try {
        // Get new Product
        let product = await Product.findOne({ _id: item }).lean().exec();
        console.log(`UPDATED PRODUCT`, product);

        // Get the quantity
        let updatedQuantity = product.stock - Number(quantity);
        console.log(`UPDATED QUANTITY:`, updatedQuantity);
        let profit = Number(quantity)*(sell_price - product.buy_price ) - discount;
        console.log("profit", profit)


        product = await Product.findByIdAndUpdate(item, { stock: updatedQuantity }, { new: true }).lean().exec();
        console.log(`UPDATED PRODUCT `, product);
        await newTrans.save();

        let user = await User.findOne({_id : created_by}).lean().exec();

        let updatedProfit = user.total_profits + profit ;
        let updatedRevenue = user.total_revenue + ( Number(quantity)*sell_price );
        let updatedOrders = user.total_orders + Number(quantity)
        console.log("totalProfits", updatedProfit);

        user.transaction.push(newTrans._id);
        user = await User.findByIdAndUpdate({_id :created_by}, {transaction: user.transaction, total_profits: updatedProfit, total_revenue: updatedRevenue, total_orders:updatedOrders } , { new: true }).lean().exec();
        let transaction = await Transaction.findByIdAndUpdate({_id: newTrans._id}, { profits: profit }, { new: true }).lean().exec();

        res.status(201).json( newTrans );
    } catch (error) {
        console.log("Error", error)
        res.status(409).json({ message: error.message });
    }
}


export const getTrans = async (req, res) => { 
    try {
        const getTransaction = await Transaction.find().populate('item');
                
        res.status(200).json(getTransaction);
    } catch (error) {
        res.status(404).json({ message: error.message });
    }
}

export const getReport = async (req, res) => {
    console.log(req.query)
    const from = req.query.fromDate.replace(/-/gi, "/")
    const to = req.query.toDate.replace(/-/gi, "/")
    
    let fromDate = new Date(from) // MM/DD/YYYY
    let toDate = new Date(to)
    toDate.setHours(toDate.getHours() + 24)
    
    console.log(`From Date ${fromDate.toDateString()}`)
    console.log(`To Date ${toDate.toDateString()}`)

    let grandTotal = 0
    let totalProfits = 0
    let totalSelling = 0

    try {
        let transaction = await Transaction.find({ 
            // timestamp: {$gte : fromDate}
            $and : [{ timestamp: {$gte : fromDate}}, { timestamp: {$lte: toDate}}] 
        }).exec();

        transaction.forEach(elem => {
            grandTotal += elem.total
            totalProfits += elem.profits
            totalSelling += elem.sell_price
        });
        
        let report = { grandTotal, totalSelling, totalProfits }

        //generate PDF of the report
        pdf.create(printReport(fromDate, toDate, transaction, grandTotal, totalProfits,totalSelling), {}).toFile('result.pdf');
        res.status(200).json({ transaction, report })
    } catch (error) {
        res.status(404).json({ message: error.message });
    }

}

export const getReportForPerDay = async (req, res) => {
    let currentDay = 10

    try {

        let reportArray = []
        let fromDate = undefined
        let toDate = undefined

        for(let i = 0; i < currentDay; i++) {
            fromDate = fromDate == undefined ? new Date() : fromDate
            toDate = fromDate.getHours() - 24
            
            let grandTotal = 0
            let totalProfits = 0
            let totalSelling = 0

            let transaction = await Transaction.find({ 
                // timestamp: {$gte : fromDate}
                $and : [{ timestamp: {$gte : fromDate}}, { timestamp: {$lte: toDate}}] 
            }).exec();

            console.log(transaction)

            transaction.forEach(elem => {
                grandTotal += elem.total
                totalProfits += elem.profits
                totalSelling += elem.sell_price
            });

            let reportObject = {
                date: fromDate.toDateString(), grandTotal, totalProfits, totalSelling
            } 

            reportArray.push(reportObject)
            console.log(reportObject)

            fromDate = toDate
        }
        
        let report = { reports: reportArray }
        res.status(200).json({ report })
    } catch (error) {
        res.status(404).json({ message: error.message });
    }
}