import express from 'express';

import { addTrans, getReport, getReportForPerDay, getTrans } from '../controllers/transactions.js';

const router = express.Router();

router.get('/', getTrans);
router.post('/', addTrans);
router.get('/report', getReport);
router.get('/report/daily', getReportForPerDay)

export default router;