import express from 'express';

import {signin, signup, updateProfile, changePassword} from '../controllers/users.js'
const router = express.Router();

router.post('/signin',signin);
router.post('/signup',signup);
router.patch('/:id', updateProfile);
router.post('/changePassword/:id', changePassword);

export default router;