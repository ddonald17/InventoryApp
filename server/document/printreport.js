const printReport = ({ transaction, grandTotal, totalProfits, totalSelling, fromDate, toDate }) => {
    const today = new Date();
return `
    <!doctype html>
    <html>
       <head>
          <meta charset="utf-8">
          <title>Report</title>
          <style>
          body {
            font-family: 'lato', sans-serif;
          }
          .container {
            max-width: 1000px;
            margin-left: auto;
            margin-right: auto;
            padding-left: 10px;
            padding-right: 10px;
          }
          
          h2 {
            font-size: 26px;
            margin: 20px 0;
            text-align: center;
            small {
              font-size: 0.5em;
            }
          }
          
          .responsive-table {
            li {
              border-radius: 3px;
              padding: 25px 30px;
              display: flex;
              justify-content: space-between;
              margin-bottom: 25px;
            }
            .table-header {
              background-color: #95A5A6;
              font-size: 14px;
              text-transform: uppercase;
              letter-spacing: 0.03em;
            }
            .table-row {
              background-color: #ffffff;
              box-shadow: 0px 0px 9px 0px rgba(0,0,0,0.1);
            }
            .col-1 {
              flex-basis: 10%;
            }
            .col-2 {
              flex-basis: 40%;
            }
            .col-3 {
              flex-basis: 25%;
            }
            .col-4 {
              flex-basis: 25%;
            }
            
            @media all and (max-width: 767px) {
              .table-header {
                display: none;
              }
              .table-row{
                
              }
              li {
                display: block;
              }
              .col {
                
                flex-basis: 100%;
                
              }
              .col {
                display: flex;
                padding: 10px 0;
                &:before {
                  color: #6C7A89;
                  padding-right: 10px;
                  content: attr(data-label);
                  flex-basis: 50%;
                  text-align: right;
                }
              }
            }
          }
          </style>
       </head>
       <body>
       <div class="container">
       <h2>reports from ${fromDate} to ${toDate}</h2>
       <ul class="responsive-table">
         <li class="table-header">
           <div class="col col-1">Customer Name</div>
           <div class="col col-2">Address</div>
           <div class="col col-3">Product</div>
           <div class="col col-4">Brand</div>
           <div class="col col-5">Quantity</div>
           <div class="col col-6">Price</div>
           <div class="col col-4">Discount</div>
           <div class="col col-4">Total</div>
           <div class="col col-4">Profits</div>


         </li>
         <li class="table-row">
           <div class="col col-1" data-label="Customer Name">${transaction.cust_name}</div>
           <div class="col col-2" data-label="Address">${transaction.cust_address}</div>
           <div class="col col-3" data-label="Product">${transaction.item.item_name}</div>
           <div class="col col-4" data-label="Brand">${transaction.item.brand}</div>
           <div class="col col-4" data-label="Quantity">${transaction.quantity}</div>
           <div class="col col-4" data-label="Price">${transaction.sell_price}</div>
           <div class="col col-4" data-label="Discount">${transaction.discount}</div>
           <div class="col col-4" data-label="Total">${transaction.Total}</div>
           <div class="col col-4" data-label="Profits">${transaction.profits}</div>


         </li>
         
       </ul>
     </div>
       </body>
    </html>
    `;
};

export default printReport;