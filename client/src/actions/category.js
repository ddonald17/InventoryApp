import axios from '../api/axios'

export const getCategory = () => async(dispatch) =>{
    try {
        const {data} = await axios.get('/category');
        dispatch ({ type:"FETCH_ALL", payload:data});
    }catch(error){
        console.log(error.message)
    }
}

export const createCategory = (category) => async(dispatch) => {
    try{
        const {data} = await axios.post("/category", category);
        dispatch({ type:'CREATE', payload:data})
    }catch(error){
        console.log(error);
    }
}

export const updateCategory = (id, category) => async (dispatch) =>{
    try{
        const {date} = await axios.patch(`/category/${id}`, category);
        dispatch({type: 'UPDATE', payload: date})
    }catch(error){
        console.log(error)
    }
}

export const deleteCategory = (id) => async (dispatch) => {
    try {
        await axios.delete(`/category/${id}`)
       dispatch ({type:'DELETE', payload:id})
    }catch(error){
        console.log(error);
    }
}