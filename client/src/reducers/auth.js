
const authReducer = (state = { authData: null }, action) => {
    switch (action.type) {
      case 'AUTH':
        localStorage.setItem('profile', JSON.stringify({ ...action?.data }));
        return { ...state, authData: action.data, loading: false, errors: null };
     
        case 'LOGOUT':
        localStorage.clear();
        return { ...state, authData: null, loading: false, errors: null };
     
        case 'PROFILE':
          return action.map((profile) => profile._id == action.payload._id ? action.payload : profile)
        
        case 'CHANGE':
          return [...state, action.payload]
          default:
        return state;
    }
  };
  
  export default authReducer;