import { combineReducers } from "redux";
import products from './products';
import transactions from "./transactions";
import auth from "./auth";
import category from "./category";

export default combineReducers({
    products,
    transactions,
    auth,
    category,

});