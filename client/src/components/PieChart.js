import { Paper, makeStyles, Typography } from '@material-ui/core';
import { Height } from '@material-ui/icons';
import React, {useEffect,useState} from 'react'
import {Doughnut} from 'react-chartjs-2'

const useStyles = makeStyles(theme => ({
  root: {
      padding: theme.spacing(1),
      margin: theme.spacing(1),
      background:'#10100f',
      backgroundColor: '#00000',
      width:'450px',
      height:'460px'
  },
 chart: {
   padding:'90px'
  }
}))

function PieChart() {

    const classes = useStyles();
    const [user, setUser] = useState(JSON.parse(localStorage.getItem('profile')));

    useEffect(() => {
      setUser(JSON.parse(localStorage.getItem('profile')));
  },[])

    const data = {
        datasets: [{
          label: 'Target',
          data: [user? user.result.total_profits : 0, user? user.result.target: 0],
          backgroundColor: [
            'rgb(255, 99, 132)',
            'rgb(54, 162, 235)',
          ],
          hoverOffset: 4
        }]
      };


    return (
        <Paper className={classes.root} >
          <Typography variant="h4" color="Secondary" align='center'>Progress</Typography>
          <div className={classes.chart}>
            <Doughnut
            data={data}
              />
          </div> 
        </Paper>
        
    )
}

export default PieChart
