import React, {useState, useEffect} from 'react';
import {Line} from 'react-chartjs-2';
import axios from '../api/axios';
import {Paper, makeStyles, Typography, } from '@material-ui/core'

const useStyles = makeStyles(theme => ({
    pageContent: {
        padding: theme.spacing(1),
        margin: theme.spacing(1),
        background:'#10100f',
        backgroundColor: '#00000',
        width:'900px',
        height:'460px'
    },
    lineChart: {
      padding:'50px'
    }
   
}))


function LineChart() {

    const [records, setRecords] = useState([]);
    const classes = useStyles();


    const fetchItems = async () =>{
        const response= await axios.get("/transaction/report/daily");
        return response.data;
    }

     useEffect(()=>{
       const getAllItem = async () =>{
        const allItem = await fetchItems();
        if(allItem) setRecords(allItem);
       };
       getAllItem();
       console.log("chart",records);
    },[]);

    const data = {
        labels: ['18/09', '19/09', '20/09', '21/09', '22/09'],
        datasets: [
          {
            label: 'Total sales',
            data: [10, 20, 50,20,70],
            fill: true,
            backgroundColor:"#2e4355",
            pointBorderColor:"#8884d8",
            pointBorderWidth:5,
            pointRadius:8,
            tension: 0.4,
          },
        ],
      };

      const options = {
        plugins:{legend:{display:false}},
        layout:{padding:{bottom:50}},
        scales: {
          y:{
            ticks:{
              color:"white",
              font:{
                size:18
              }
            },
            grid:{
              color:"#243240",
              // display:false
            }
          },
          x:{
            ticks:{
              color:"white",
              font:{
                size:18
              }
            }
          }
        },
      };
    console.log(records);

    
    return (
        <Paper className={classes.pageContent}>
          <Typography variant="h4" align='center' color="secondary">Sales</Typography>

          <div className={classes.lineChart}>
            <Line data={data} options={options}/>
          </div>
         </Paper>

    )
}

export default LineChart
