import React , {useEffect, useState} from 'react'
import { Grid, } from '@material-ui/core';
import Controls from "../../components/controls/Controls";
import { useForm, Form } from '../../components/useForm';
import axios from '../../api/axios';


const user = JSON.parse(localStorage.getItem('profile'));
// console.log(user.result._id);

const initialValues ={
    item_name:'',
    brand: '',
    category:'',
    buy_price:'',
    sell_price:'',
    stock:'',
    created_by:user? user.result._id : '613f8fcec29ba8368002d0e7' 
}


function AddItemForm(props) {

    const { addOrEdit, recordForEdit } = props
    const [records, setRecords] = useState([]);


    const handleSubmit = e => {
        e.preventDefault();
        addOrEdit(values, resetForm)
    }

    const {
        values,
        setValues,
        handleInputChange,
        resetForm
    } = useForm(initialValues);

    useEffect(()=> {
        if(recordForEdit != null)
        setValues({...recordForEdit})
    },[recordForEdit])

    const fetchItems = async () =>{
        const response= await axios.get("/category");
        return response.data;
    }

     useEffect(()=>{
       const getAllItem = async () =>{
        const allItem = await fetchItems();
        if(allItem) setRecords(allItem);
       };
       getAllItem();
    },[])

    

    return (
        <Form onSubmit={handleSubmit}>
             <Grid container>
                <Grid item xs={6}>
                    <Controls.Input
                        name="item_name"
                        label="Product Name"
                        value={values.item_name}
                        onChange={handleInputChange}
                    />
                    <Controls.Input
                        name="brand"
                        label="Brand"
                        value={values.brand}
                        onChange={handleInputChange}
                    />
                     <Controls.Select
                        name="category"
                        label="Category"
                        value={records._id}
                        onChange={handleInputChange}
                        options={records}
                    />
                   
                    <div>
                        <Controls.Button
                            type="submit"
                            text="Submit" />
                        <Controls.Button
                            text="Reset"
                            color="default"
                            onClick={resetForm} />
                    </div>
                </Grid>
                <Grid item xs={6}>
                    <Controls.Input
                        label="Buy Price"
                        name="buy_price"
                        value={values.buy_price}
                        onChange={handleInputChange}
                    />
                    <Controls.Input
                        label="MRP"
                        name="sell_price"
                        value={values.sell_price}
                        onChange={handleInputChange}
                    />
                    <Controls.Input
                        label="Stock"
                        name="stock"
                        value={values.stock}
                        onChange={handleInputChange}
                    />
                </Grid>

             </Grid>
        </Form>
    )
}

export default AddItemForm
