import React, {useState, useEffect} from 'react';
import {Paper, makeStyles, Typography, Grid, Button, Container} from '@material-ui/core';
import Controls from "../../components/controls/Controls";
import { profile } from '../../actions/auth';
import { useDispatch } from 'react-redux';


const useStyles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(3),
        margin: theme.spacing(5),
    },
    form: {
        width: '100%', 
        marginTop: theme.spacing(3),
      },
}))

const initialState = { firstName: '', lastName: '', email: '', target:'' };


function Profile() {

    const classes = useStyles();
    const [form, setForm] = useState(initialState);
    const dispatch = useDispatch();
    const [user, setUser] = useState(JSON.parse(localStorage.getItem('profile')));
    var name = user.result.name.split(" ");
    console.log(user);

    useEffect(() => {
        setUser(JSON.parse(localStorage.getItem('profile')));
    },[])


    const handleInputChange =(e) => {
        setForm({ ...form, [e.target.name]: e.target.value })
    };
    console.log(form);

    const handleSubmit = ( e) => {
        e.preventDefault();
        dispatch(profile(user.result._id, form))          
        
    };

    return (
        <>
        <Container component="main" >
        <Paper className={classes.paper}>
            <Typography variant="h3"  color="initial">Profile</Typography>
            <form className={classes.form} onSubmit={handleSubmit}>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                <Controls.Input
                 name = "firstName"
                 label="First Name"
                 defaultValue = {name[0]}
                 onChange={handleInputChange} 
              />
              </Grid>
              <Grid item xs={12}>
              <Controls.Input
                  label="Last Name"
                  name="lastName"
                 defaultValue = {name[1]}
                  onChange={handleInputChange}
                />
              </Grid>
              <Grid item xs={12}>
                <Controls.Input
                 name = "email"
                 label="Email"
                 defaultValue = {user.result.email}
                 onChange={handleInputChange} 
              />
              </Grid>
              <Grid item xs={12}>
              <Controls.Input
                  label="Target Profits"
                  name="target"
                 defaultValue = {user.result.target}
                  onChange={handleInputChange}
                />   
                </Grid>
                <Grid item xs={12}>
                    <Controls.Button
                    text="Update Information"
                    type="submit"
                    fullWidth
                    />
                </Grid>
            </Grid>
            </form>
          
        </Paper>
        </Container>

        </>

    )
}

export default Profile
