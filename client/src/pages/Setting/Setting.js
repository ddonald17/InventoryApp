import React, {useState, useEffect} from 'react';
import {Paper, makeStyles, Typography, Grid, Container} from '@material-ui/core';
import Controls from "../../components/controls/Controls";
import { changePassword } from '../../actions/auth';
import { useDispatch } from 'react-redux';

const useStyles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(3),
        margin: theme.spacing(5),
    },
    form: {
        width: '100%', 
        marginTop: theme.spacing(3),
      },
}))

const initialState = { password: '', newPassword: '', COnfirmPassword: '' };


function Setting() {
    const classes = useStyles();
    const [form, setForm] = useState(initialState);
    const [showPassword, setShowPassword] = useState(false);
    const [user, setUser] = useState(JSON.parse(localStorage.getItem('profile')));
    const dispatch = useDispatch();

    const handleShowPassword = () => setShowPassword(!showPassword);

    useEffect(() => {
        setUser(JSON.parse(localStorage.getItem('profile')));
    },[])

    const handleInputChange =(e) => {
        setForm({...form, [e.target.name]: e.target.value})
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        dispatch(changePassword(user.result._id, form))          
    };
    return (
        <>
        <Container component="main" >
        <Paper className={classes.paper}>
            <Typography variant="h3"  color="initial">Change Password</Typography>
            <form className={classes.form} onSubmit={handleSubmit}>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                <Controls.Input
                 name = "oldPassword"
                 label="Old Password"
                 onChange={handleInputChange} 
                 type={showPassword ? 'text' : 'password'}
                 handleShowPassword = {handleShowPassword}
              />
              </Grid>
             
              <Grid item xs={12}>
                <Controls.Input
                 name = "newPassword"
                 label="New Password"
                 type={showPassword ? 'text' : 'password'}
                 onChange={handleInputChange} 
               
              />
              </Grid>

              <Grid item xs={12}>
              <Controls.Input
                  label="Confirm Password"
                  name="confirmPassword"
                 type={showPassword ? 'text' : 'password'}
                  onChange={handleInputChange}
                />   
                </Grid>
                <Grid item xs={12}>
                    <Controls.Button
                    text="Change password"
                    type="submit"
                    fullWidth
                    />
                </Grid>
            </Grid>
            </form>
          
        </Paper>
        </Container>

        </>
    )
}

export default Setting
