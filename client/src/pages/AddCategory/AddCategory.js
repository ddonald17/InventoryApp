import React,{useState, useEffect} from 'react';
import { Paper, makeStyles, TableBody, TableRow, TableCell } from '@material-ui/core';
import AddCategoryForm from './AddCategoryForm';
import AddIcon from '@material-ui/icons/Add';
import Controls from "../../components/controls/Controls";
import { Search } from "@material-ui/icons";
import {Toolbar, InputAdornment } from  '@material-ui/core';
import Popup from "../../components/Popup";
import  useTable from '../../components/useTable';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import { useSelector, useDispatch } from 'react-redux';
import { getCategory, createCategory, deleteCategory, updateCategory } from '../../actions/category';
import axios from '../../api/axios';


const useStyles = makeStyles(theme => ({
    pageContent: {
        padding: theme.spacing(3),
        margin: theme.spacing(5),
    },
    searchInput: {
        width: '75%'
    },
    newButton: {
        position: 'absolute',
        right: '10px'
    }
}))

const headCells = [
    { id: 'Category_name', label: 'Category Name' },
    { id: 'category_description', label: 'Category Description' },
    { id: 'tax', label: 'Tax (%)' },
    { id: 'actions', label: 'Actions', disableSorting: true }
];


function AddCategory() {

    const classes = useStyles();
    const category = useSelector((state) => state.category);
    const [recordForEdit, setRecordForEdit] = useState(null);
    const [openPopup, setOpenPopup] = useState(false);
    const [records, setRecords] = useState([]);
    const [edit, setEdit] = useState(false);
    const [filterFn, setFilterFn] = useState({ fn: items => { return items; } })
    const dispatch = useDispatch();
    const { TblContainer, TblHead,  TblPagination, recordsAfterPagingAndSorting } = useTable( records, headCells, filterFn);
    console.log("category" ,category)
   
   
    const handleSearch = e => {
        let target = e.target;
        setFilterFn({
            fn: items => {
                if (target.value === "")
                    return items;
                else
                    return items.filter(x => x.item_name.toLowerCase().includes(target.value))
            }
        })
    }


    const removeItemHandler = async (record) => {
       dispatch(deleteCategory(record._id));
    }
    

    const addOrEdit = (item , resetForm) => {
        if (edit)
           dispatch(updateCategory(item._id, item))          
        else
           dispatch(createCategory(item));
        resetForm();
        setRecordForEdit(null);
        setEdit(false);
        setRecords(category);
        setOpenPopup(false);  
    }


    const openInPopup = item => {
        setRecordForEdit(item)
        setOpenPopup(true)
    }


    const fetchItems = async () =>{
        const response= await axios.get("/category");
        return response.data;
    }

     useEffect(()=>{
       const getAllItem = async () =>{
        const allItem = await fetchItems();
        if(allItem) setRecords(allItem);
       };
       getAllItem();
    },[])
    

   
    return (
        <>
        <Paper className={classes.pageContent}>
                <Toolbar>
                    <Controls.Input
                        label="Search Category"
                        className={classes.searchInput}
                        InputProps={{
                            startAdornment: (<InputAdornment position="start">
                                <Search />
                            </InputAdornment>)
                        }}
                        onChange={handleSearch}
                    />
                <Controls.Button
                        text="Add New"
                        startIcon={<AddIcon />}
                        className={classes.newButton}
                        onClick={() => { setOpenPopup(true); setRecordForEdit(null); }}
                 />
                </Toolbar>

                 <TblContainer>
                        <TblHead />
                        <TableBody>
                            {
                                recordsAfterPagingAndSorting().map( item =>(
                                    <TableRow key={item._id}>
                                        <TableCell>{item.item_name}</TableCell>
                                        <TableCell>{item.category_description}</TableCell>
                                        <TableCell>{item.tax}</TableCell>
                                        <TableCell>
                                        <Controls.ActionButton
                                            color="primary"
                                            onClick={() => { openInPopup(item) ; setEdit(true); }}>
                                            <EditOutlinedIcon fontSize="small" />
                                        </Controls.ActionButton>
                                        <Controls.ActionButton
                                            color="secondary" 
                                            onClick={()=>removeItemHandler(item)}>
                                            <DeleteOutlineIcon fontSize="small" />
                                        </Controls.ActionButton>
                                    </TableCell>
                                    </TableRow>)
                                    )
                            }
                        </TableBody>
                 </TblContainer>
                 <TblPagination />
                
            </Paper>

            <Popup
             title="Add Category"
             openPopup={openPopup}
             setOpenPopup={setOpenPopup}
                  >
                    <AddCategoryForm 
                    recordForEdit={recordForEdit}
                    addOrEdit={addOrEdit} />
            </Popup>
        </>
    )
}

export default AddCategory
