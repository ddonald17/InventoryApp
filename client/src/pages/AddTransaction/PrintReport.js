import { Grid, Paper , makeStyles, Typography} from '@material-ui/core'
import React, { useState } from 'react';
import DateMomentUtils from '@date-io/moment';
import {
  DatePicker,
  MuiPickersUtilsProvider,
} from '@material-ui/pickers';
import {  Form } from '../../components/useForm';
import Controls from '../../components/controls/Controls';
import { useDispatch, useSelector } from 'react-redux';
import { getReport } from '../../actions/transactions';
import * as moment  from 'moment';



const useStyles = makeStyles(theme => ({
    pageContent: {
        padding: theme.spacing(3),
        margin: theme.spacing(5),
    },
   
    newButton: {
        position: 'absolute',
        right: '10px'
    }
}))


function PrintReport() {
    const classes = useStyles();
    const [fromDate, setFromData] = useState(new Date());
    const [toDate, setToData] = useState(new Date());
    const dispatch = useDispatch();

    console.log(moment(fromDate._d).format('MM/DD/YYYY'));

    const handleSubmit = (e) => {
        e.preventDefault();
      let  trans=dispatch(getReport(moment(fromDate._d).format('MM/DD/YYYY'), moment(toDate._d).format('MM/DD/YYYY')))
      console.log("t",trans)
    };


    return (
       <>
       <Paper className= {classes.pageContent}>
        <MuiPickersUtilsProvider utils={DateMomentUtils}>
            <Typography variant="h3" color="initial">Print Report</Typography>
            <Form onSubmit={handleSubmit} >
                <Grid container>
                    <Grid item xs={6}> 
                     <DatePicker disableFuture label='From Date' value={fromDate} onChange={setFromData}  />  
                    </Grid>
                    <Grid item xs={6}>
                     <DatePicker disableFuture label='To Date' value={toDate} onChange={setToData} />  
                    </Grid>
                    <Grid item xs={12}>
                    <div>
                        <Controls.Button
                            type="submit"
                            text="Submit" />
                    </div>
                    </Grid>
                </Grid>
            </Form>
         </MuiPickersUtilsProvider>
         </Paper>
       </>
    )
}

export default PrintReport
